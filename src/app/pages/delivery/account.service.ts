import { Injectable } from '@angular/core';
import { AccountDA } from './account.da.service';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private accountDA: AccountDA) {}

  getCustomerAccountsAndTransactions(id: number): Promise<any> {
    return this.accountDA.getCustomerAccountsAndTransactions(id);
  }
  getAllCustomers(): Promise<any> {
    return this.accountDA.getAllCustomers();
  }

  createCustomerAccount(customerId: number, initialCredit: number) {
    return this.accountDA.createCustomerAccount(customerId, initialCredit);
  }
}
