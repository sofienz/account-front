import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AccountDA {
  private API_URL: string = environment.API_URL + '/v1';
  constructor(private http: HttpClient) {}

  getCustomerAccountsAndTransactions(id: number): Promise<any> {
    return this.http.get<any>(this.API_URL + '/customer/' + id).toPromise();
  }
  getAllCustomers(): Promise<any> {
    return this.http.get<any>(this.API_URL + '/customer').toPromise();
  }
  createCustomerAccount(customerId: number, initialCredit: number) {
    return this.http
      .post<any>(this.API_URL + '/account', { customerId, initialCredit })
      .toPromise();
  }
}
