import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from 'src/app/_helpers/utils';
import { AccountService } from './account.service';
import { DatePipe } from '@angular/common';
import { Customer } from 'src/app/core/data/user';
import { ThisReceiver } from '@angular/compiler';
import { Account } from 'src/app/core/data/account';
import { Transaction } from 'src/app/core/data/transaction';

@Component({
  selector: 'account-cmp',
  templateUrl: 'account.component.html',
})
export class AccountComponent implements OnInit {
  pageSize: number = 10;
  page: number = 1;
  customersList: Customer[] = [];
  selectedCustomerId: number = 0;
  selectedCustomer: Customer = { accounts: [] };
  credit: number = 0;
  selectedAccountID: number = 0;
  selectedTransactions: Transaction[] = [];
  constructor(
    private modalService: NgbModal,
    private deliveryService: AccountService,
    private utilsService: UtilsService,
    private router: Router,
    public datepipe: DatePipe
  ) {}
  async ngOnInit() {
    this.customersList = await this.deliveryService.getAllCustomers();
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }
  customerChange() {
    const result = this.customersList.find(
      (customer) => customer.id == this.selectedCustomerId
    );
    if (result) this.selectedCustomer = result;
  }
  showAccountTransaction(account: Account, popup: any) {
    if (account.transactions) this.selectedTransactions = account.transactions;
    if (account.id) this.selectedAccountID = account.id;
    this.open(popup);
  }
  async createAccount() {
    await this.deliveryService.createCustomerAccount(
      this.selectedCustomerId,
      this.credit
    );
    this.customersList = await this.deliveryService.getAllCustomers();
    const result = this.customersList.find(
      (customer) => customer.id == this.selectedCustomerId
    );
    if (result) this.selectedCustomer = result;
  }
}
