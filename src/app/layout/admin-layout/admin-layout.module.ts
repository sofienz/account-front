import { NgModule } from '@angular/core';

import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AccountComponent } from 'src/app/pages/delivery/account.component';

@NgModule({
  imports: [CommonModule, FormsModule, NgbModule],
  declarations: [AccountComponent],
  providers: [DatePipe],
})
export class AdminLayoutModule {}
