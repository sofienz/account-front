import { __assign } from 'tslib';
import { Transaction } from './transaction';

export interface Account {
  id?: number;
  balance?: number;
  creationDate?: string;
  transactions?: Transaction[];
}
