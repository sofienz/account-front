import { __assign } from 'tslib';

export interface Transaction {
  id?: string;
  amount?: number;
  transactionDate?: string;
}
