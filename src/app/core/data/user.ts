import { __assign } from 'tslib';
import { Account } from './account';

export interface Customer {
  id?: number;
  name?: string;
  surname?: string;
  accounts: Account[];
}
