import { Component, OnInit } from '@angular/core';

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {
    path: '/account',
    title: 'account',
    icon: 'nc-single-02',
    class: '',
  },
];

@Component({
  selector: 'sidebar-cmp',
  templateUrl: 'sidebar.component.html',
})
export class SidebarComponent implements OnInit {
  public menuItems: any[] = [];
  constructor() {}
  ngOnInit() {
    this.menuItems = ROUTES;
  }
}
